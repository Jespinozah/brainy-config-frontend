import {HttpClient, json} from 'aurelia-fetch-client';
import {ConfigVar} from '../models/config-var.js';
import {inject} from 'aurelia-framework';

@inject(HttpClient)
export class WebApi {

  constructor(http) {
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('/configApi');
    });

    this.http = http;
  }

  getConfigVarList(){
    return this.http.fetch('/configuration')
        .then(response => response.json())
        .then((json) => {
          let configVars = [];
          for( let key in json) {
            let keyObj = { key: key };
            keyObj.values = configVars.concat(json[key].map(x => new ConfigVar(x.key, x.value)));
            configVars.push(keyObj);
          }
          return configVars;
     });
  }

  saveConfigVars(configVars) {
    let jsonObj = {};
    for (let item of configVars) {
      jsonObj[item.key] = item.values;
    }
    return this.http.fetch('/configuration', {
        method: 'put',
        body: json(jsonObj)
    });
  }

}
